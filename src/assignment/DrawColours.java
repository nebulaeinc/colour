package assignment;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class DrawColours extends JFrame {
	
	private static final int lineHeight = 60; // height of vertical line for each colour
	
	private int size; // number of colours or width of image
	private float[][] colours; // a size x 3 matrix representing colours
	private int[] permutation; // the order in which to display the colours
	
	public DrawColours(float[][] colours, int[] permutation) {
		
        super("Colours"); // JFrame constructor with "Colours" as title
        
        assert colours.length == permutation.length; 
        assert colours[0].length == 3; // a colour needs to be defined by 3 values
        
        this.colours = colours;
        this.setDSize(colours.length);
        this.permutation = permutation;

        setSize(getDSize(), lineHeight); // set width and height of window
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
    }
	
	/**
	 * Draw the colours as vertical lines according to the permutation order.
	 * @param g
	 */
	public void drawLines(Graphics g) {
    	
        Graphics2D g2d = (Graphics2D) g;
        
        for (int i = 0; i < getDSize(); ++i) {
        	g2d.setColor(new Color(colours[permutation[i]][0], colours[permutation[i]][1], colours[permutation[i]][2]));
        	g2d.drawLine(i, 0, i, lineHeight);
        }
 
    }
 
    public void paint(Graphics g) {
        super.paint(g);
        drawLines(g);
    }
    
    /**
     * Save the colour permutation to a PNG file
     */    
    public void savePNG() {
    	// Save image
        BufferedImage bImg = new BufferedImage(getDSize(), lineHeight, BufferedImage.TYPE_INT_RGB);
        Graphics2D cg = bImg.createGraphics();
        drawLines(cg);
        try {
            if (ImageIO.write(bImg, "png", new File("output_image.png")))
            {
                System.out.println("Image Saved");
            }
	    } catch (IOException e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	    }
    }
    
    
public static void main(String[] args) {
    	
    	int numberColours = 1000;
    	
    	float[][] colours = new float[numberColours][3];
    	int[] permutation = new int[numberColours];   	  
    	
    	// TODO Read colours from file and compute some permutation
    	// Read from file and have output as a populated colours [][] float
    	
    	
    	String filePath = "resources/colours.txt";
    	File file = new File(filePath);
    	Boolean pickuplength = false;
    	int size;
    	int NumRGB = 0;
		try{
	    	Scanner sc  = new Scanner(file).useDelimiter("\\s");
	    	
	    	while(sc.hasNext()){
	    		if(NumRGB < numberColours ){	 	    		
	    			if(sc.hasNextFloat()){
		    			if(pickuplength == false){
		    				size = sc.nextInt();
							//System.out.println("NuMBER: "+ size);
							pickuplength = true;
		    			}
		    			else{
		    				
		    				for(int i = 0; i <3 ; i++){
		    					colours[NumRGB][i] = sc.nextFloat();
		    					//System.out.print( "  " + colours[NumRGB][i] + " " );
		    				}
		    				NumRGB += 1;
		    				//System.out.println();
		    				
		    			}
		    		}
		    		else{
		    			sc.next();
		    			//do nothing	    			
		    		}	
	    		
	    		
	    		}else{
	    			break;
	    		}
	    		    		
	    	}			
		}
		catch(FileNotFoundException e){
			
		}
		int[] permutation2 = new int[numberColours];	
		int[] permutation3 = new int[numberColours];
		int[] permutation4 = new int[numberColours];
		int[] permutation5 = new int[numberColours];		
		
		colorManagement cm = new colorManagement(colours);				
		permutation = cm.randomCalc(numberColours);
		
		System.arraycopy(permutation, 0, permutation2, 0, numberColours);
		System.arraycopy(permutation, 0, permutation3, 0, numberColours);
		System.arraycopy(permutation, 0, permutation4, 0, numberColours);
		System.arraycopy(permutation, 0, permutation5, 0, numberColours);		
		
		long startTime = System.currentTimeMillis();
		permutation2 = cm.simpleRandomSearch(1000, numberColours);
		long endTime = System.currentTimeMillis();
    	System.out.println("simpleRandomSearch "+(endTime - startTime) + " ns");
    	System.out.println();
    	
    	startTime = System.currentTimeMillis();
		permutation3 = cm.HillClimbing2opt(numberColours, permutation3);
		endTime = System.currentTimeMillis();
    	System.out.println("HillClimbing2opt "+(endTime - startTime) + " ns");
    	System.out.println();
    	
    	
    	startTime = System.currentTimeMillis();
		permutation4 = cm.multipleStartsHill(numberColours, 20, permutation4);
		endTime = System.currentTimeMillis();
    	System.out.println("multipleStartsHill "+(endTime - startTime) + " ns");
    	System.out.println();
    	
    	startTime = System.currentTimeMillis();
		//permutation5 = cm.itterLocalSearch(numberColours, permutation5);
		endTime = System.currentTimeMillis();
    	System.out.println("itterLocalSearch "+(endTime - startTime) + " ns");
    	System.out.println();
		
		double fitness = cm.eucledianCalc(numberColours,permutation2);
		System.out.println("The fitness is of simpleRandomSearch " + fitness);
		
		fitness = cm.eucledianCalc(numberColours,permutation3);
		System.out.println("The fitness is of HillClimbing2opt " + fitness);
			
		fitness = cm.eucledianCalc(numberColours,permutation4);
		System.out.println("The fitness is of multipleStartsHill " + fitness);
		
		fitness = cm.eucledianCalc(numberColours,permutation5);
		System.out.println("The fitness is of itterLocalSearch " + fitness);
		
	    final DrawColours displayColours = new DrawColours(colours, permutation3);
    	
    	// Display image
    	SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                displayColours.setVisible(true);
            }
        });
        
        // Save image
    	displayColours.savePNG();
    }

	public int getDSize() {
		return size;
	}

	public void setDSize(int size) {
		this.size = size;
	}
    
}