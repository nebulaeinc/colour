package assignment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import javax.swing.SwingUtilities;

public class colorManagement {

	private float[][] colours;
	
	public colorManagement(float [][] colours){
		this.colours = colours;
	}
	
	//generate unique random values to build a random permutation
	public int[] randomCalc(int length){
		ArrayList<Integer> ordered = new ArrayList<>(length);
		int[] permutation = new int[length]; 		
		for(int i = 0; i < length ; i++){
			ordered.add(i);
		}
		Collections.shuffle(ordered);
		int iterator = 0;
		for (int temp : ordered) {
			permutation[iterator] = temp;
			iterator++;
		}

		
		return permutation;
	}

	public double eucledianCalc(int length, int[] permutation){	
		double sum = 0; //sum of all distances between adjacent elements of the given permutation
		for(int i = 0; i < length-1; i++){
			
			
			int left = permutation[i];
			int right = permutation[i + 1];
			
			float r = colours[left][0];
			float g = colours[left][1];
			float b = colours[left][2];
			
			float r2 = colours[right][0];
			float g2 = colours[right][1];
			float b2 = colours[right][2];
			
			
			float diffR = (r - r2) * (r - r2);
			float diffG = (g - g2) * (g - g2);
			float diffB = (b - b2) * (b - b2);
			
			float cumalitive = diffR + diffG + diffB;
			double finalDiff = Math.sqrt(cumalitive);
			sum += finalDiff;
		}		
		return sum;
	}
	
	public int[] simpleRandomSearch(int tries, int length){
		int[] permutation = randomCalc(length);
		int[] tempPermutation;
		double tempDistance;
		double cumDistance = eucledianCalc(length, permutation);
		for(int i = 0; i < tries; i++){
			tempPermutation = randomCalc(length);
			tempDistance = eucledianCalc(length, tempPermutation);
			if(cumDistance >= tempDistance){
				permutation = tempPermutation; 
				cumDistance = tempDistance;
			}			
		}
		return permutation;
	}	
	
	
	public int[] HillClimbingRandom(int length, int [] originPermutation){
		int [] permutation = new int[length];	
		for(int i = 0; i<length; i++){
			int val = originPermutation[i];
			permutation[i] = val;
		}		
		double distance;
		double newDistance;		
		//this algorithm keeps swapping numbers at random until
		//all the swaps have lead to a local optimum swap		
		//one option is too allow for infinite swaps 
		//the other option is to take two numbers ---> check for swap --->commit them to new solution
		Random r = new Random();		
		int maxNonItt = 2000;
		int noChange = 0;
		for(int i = 0; i < 10000000; i++){
			int val1 = r.nextInt(length);
			int val2 = r.nextInt(length);
			
			while(val1 == val2){
				val2 = r.nextInt(length);
			}
			
			//problem is they are referenced 
			//c	
			int swap1 = permutation[val1]; // at 10 swap is c
				//x
			int swap2 = permutation[val2]; // at 20 swap is x			
			permutation[val1] = swap2; // at 10 val is 99
			permutation[val2] = swap1; // at 20 val is 44	
			
			distance =  eucledianCalc(length, originPermutation);
			newDistance =  eucledianCalc(length, permutation);
			if(newDistance < distance){
				noChange = 0; 
				originPermutation[val1] = swap2; // at 10 val is 99
				originPermutation[val2] = swap1;
			}else{ //revert back
				noChange++;
				permutation[val1] = swap1; // at 10 val is 99
				permutation[val2] = swap2;
			}
			
			if(noChange >= maxNonItt){//not worth pursuing break from hillsearch
				break;
			}
		}
		
		return permutation;
	}

	
	
	
	public int[] HillClimbing2opt(int length, int [] originPermutation){
		int [] permutation = new int[length];		
		for(int i = 0; i<length; i++){
			int val = originPermutation[i];
			permutation[i] = val;
		}
		
		double distance;
		double newDistance;		
		//this algorithm keeps swapping numbers at random until
		//all the swaps have lead to a local optimum swap		
		//one option is too allow for infinite swaps 
		//the other option is to take two numbers ---> check for swap --->commit them to new solution
		Random r = new Random();		
		int maxNonItt = 1000;
		int noChange = 0;
		for(int i = 0; i < 10000000; i++){
			int val1 = r.nextInt(length);
			int val2 = r.nextInt(length);
			
			while(val1 == val2){ //ensure the two values are never the same
				val2 = r.nextInt(length);
			}			
			if(val1 > val2){
				//organise for ease of use later on so val1 is always the lower end
				int temp = val2;
				val2 = val1;
				val1 = temp;
			}		
			int swap1 = 0;
			int swap2 = 0;
			//2-opt swap --- work from the outside towards the centre
			//manage for both even and odd sets
			int diff = val2 - val1;
			for(int j = 0; j <= diff; j++){
				if(val1+j >= val2-j){
					break;
				}else{
					//c
					
					swap1 = permutation[val1 + j]; // at 10 swap is c
					//x
					swap2 = permutation[val2 - j]; // at 20 swap is x			
					permutation[val1 + j] = swap2; // at 10 val is 99
					permutation[val2 - j] = swap1; // at 20 val is 44 	
					
				}
			}		
			distance =  eucledianCalc(length, originPermutation);
			newDistance =  eucledianCalc(length, permutation);
			if(newDistance < distance){				
				System.arraycopy(permutation, 0, originPermutation, 0, length); //fastest approach
				noChange = 0; 
			}else{ //revert back
				noChange++;
				System.arraycopy(originPermutation, 0, permutation, 0, length); //fastest approach
			}			
			if(noChange >= maxNonItt){//not worth pursuing break from hillsearch
				break;
			}			
		}		
		return permutation;
	}
	
	
	
	public int[] multipleStartsHill(int length, int tries, int[] permutation){
		int [] tempPermutation = new int [length];
		for(int i = 0; i < tries; i ++){
			tempPermutation = randomCalc(length);
			tempPermutation = HillClimbing2opt(length, tempPermutation);			
			double fitnessOfTemp = eucledianCalc(length, tempPermutation);
			double fitnessOfCurr = eucledianCalc(length, permutation);			
			if(fitnessOfTemp <= fitnessOfCurr){
				System.arraycopy(tempPermutation, 0, permutation, 0, length);
			}		
		}		
		return permutation;
	}
	
	
	
	public int[] itterLocalSearch(int length, int [] originPermutation){
		int [] permutation = new int[length];
		int [] permutation2 = new int[length];
		for(int i = 0; i<length; i++){
			int val = originPermutation[i];
			permutation[i] = val;
			permutation2[i] = val;
		}
		
		double distance;
		double newDistance;
		double newDistance2;
		//this algorithm keeps swapping numbers at random until
		//all the swaps have lead to a local optimum swap		
		//one option is too allow for infinite swaps 
		//the other option is to take two numbers ---> check for swap --->commit them to new solution
		Random r = new Random();		
		int maxNonItt = 500;
		int noChange = 0;
		for(int i = 0; i < 10000000; i++){
			int val1 = r.nextInt(length);
			int val2 = r.nextInt(length);
			int val3 = r.nextInt(length);
			int val4 = r.nextInt(length);
			
			while(val1 == val2){ //ensure the two values are never the same
				val2 = r.nextInt(length);
			}			
			if(val1 > val2){
				//organise for ease of use later on so val1 is always the lower end
				int temp = val2;
				val2 = val1;
				val1 = temp;
			}		
			
			while(val3 == val4){ //ensure the two values are never the same
				val4 = r.nextInt(length);
			}			
			if(val3 > val4){
				//organise for ease of use later on so val1 is always the lower end
				int temp = val4;
				val4 = val1;
				val3 = temp;
			}		
			
			//
			permutation = twoOpt(permutation, val1,val2);
			permutation = twoOpt(permutation, val3, val4); 			
			permutation = HillClimbing2opt(length, permutation);
			
			distance =  eucledianCalc(length, originPermutation);
			newDistance =  eucledianCalc(length, permutation);
			if(newDistance < distance ){				
				System.arraycopy(permutation, 0, originPermutation, 0, length); //fastest approach
				noChange = 0; 
			}else{ //revert back
				noChange++;
				System.arraycopy(originPermutation, 0, permutation, 0, length); //fastest approach
			}			
			if(noChange >= maxNonItt){//not worth pursuing break from hillsearch
				break;
			}			
		}	
		return permutation;
	}
	
	
	
	public int[] twoOpt(int[] permutation, int val1, int val2){		
		int swap1 = 0;
		int swap2 = 0;
		//2-opt swap --- work from the outside towards the centre
		//manage for both even and odd sets
		int diff = val2 - val1;
		for(int j = 0; j <= diff; j++){
			if(val1+j >= val2-j){
				break;
			}else{
				//c
				
				swap1 = permutation[val1 + j]; // at 10 swap is c
				//x
				swap2 = permutation[val2 - j]; // at 20 swap is x			
				permutation[val1 + j] = swap2; // at 10 val is 99
				permutation[val2 - j] = swap1; // at 20 val is 44 	
				
			}
		}			
		return permutation;
	}
	
	
}
